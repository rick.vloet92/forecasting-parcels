# libraries

import pandas as pd 
from pytrends.request import TrendReq



pytrend = TrendReq()
# df = pytrend.trending_searches(pn='united_kingdom')
# df.head()

pytrend.build_payload(kw_list=['sendcloud'], geo='NL', timeframe='today')
# Interest by Region
df = pytrend.interest_over_time()
df.head(10)